# Planar Reflection

Following Guidev's tutorial on implementing reflection — more specifically, planar reflection — this project was created to assist learning and understanding by necessitating attention to, and concentration on, the topic and allowing for tinkering.

Basically, I just wanted reflections in New Chess. Preferably on top of a water shader /w its distortion effect.

![Showcase GIF](Media/showcase.gif)

A note found in the comment section:

>For anyone wondering why this doesn't work for HDRP or LWRP (URP now) this is because rendering technique has changed and OnPostRender, OnRender, OnPreRender methods will not work with camera. 
>You have to either figure out how to use custom render pipeline or just render the reflection onto a separate texture and add this texture onto a custom shader (like i did).
>Just don't use OnPostRender, OnRender and stuff like that, move the logic into Update, even though it's better to learn how to use the custom render pipeline approach and replace Update with camera callbacks to make sure the reflection is rendered properly on each frame

## Branches

#### master

**Description:** The original tutorial.

#### without-GL

**Description:** An implementation that doesn't use the C# GL library and the stencil buffer. Rather than rendering the texture to a new screen-sized quad, it's sampled by the floor shader/material in screen space.