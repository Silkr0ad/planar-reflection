﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanarReflectionManager : MonoBehaviour {
    Camera m_ReflectionCamera;
    Camera m_MainCamera;

    public GameObject m_ReflectionPlane;
    public bool mirrorForward = true;
    public bool mirrorUp = true;
    public bool mirrorPosition = true;
    
    [Range(0f, 1f)] public float m_ReflectionFactor = 0.5f;

    public Material m_FloorMaterial;

    RenderTexture m_RenderTarget;

    private void Start() {
        GameObject reflectionCameraGO = new GameObject("Reflection Camera");
        m_ReflectionCamera = reflectionCameraGO.AddComponent<Camera>();
        m_ReflectionCamera.enabled = false;

        m_MainCamera = Camera.main;

        m_RenderTarget = new RenderTexture(Screen.width, Screen.height, 24);    
    }

    private void Update() {
        Shader.SetGlobalFloat("_ReflectionFactor", m_ReflectionFactor);
    }

    private void OnPostRender() {
        RenderReflection();
    }

    private void RenderReflection() {
        m_ReflectionCamera.CopyFrom(m_MainCamera);

        Vector3 camFwdWS = m_MainCamera.transform.forward;
        Vector3 camUpWS  = m_MainCamera.transform.up;
        Vector3 camPosWS = m_MainCamera.transform.position;

        // Transform the cam vectors to plane space
        Vector3 camFwdPS = m_ReflectionPlane.transform.InverseTransformDirection(camFwdWS);
        Vector3 camUpPS  = m_ReflectionPlane.transform.InverseTransformDirection(camUpWS);
        Vector3 camPosPS = m_ReflectionPlane.transform.InverseTransformPoint(camPosWS);

        // Mirror the vectors. (The bools are there for visualization.)
        camFwdPS.y *= mirrorForward ? -1f : 1f;
        camUpPS.y  *= mirrorUp ? -1f : 1f;
        camPosPS.y *= mirrorPosition ? -1f : 1f;

        // Transform the vectors back into world space
        camFwdWS = m_ReflectionPlane.transform.TransformDirection(camFwdPS);
        camUpWS = m_ReflectionPlane.transform.TransformDirection(camUpPS);
        camPosWS = m_ReflectionPlane.transform.TransformPoint(camPosPS);

        // Set reflection camera pos and rot
        m_ReflectionCamera.transform.position = camPosWS;
        m_ReflectionCamera.transform.LookAt(camPosWS + camFwdWS, camUpWS);

        // Set the render target for the reflection camera
        m_ReflectionCamera.targetTexture = m_RenderTarget;

        // Render the reflection camera
        m_ReflectionCamera.Render();

        // Draw the full-screen quad
        DrawQuad();
    }

    private void DrawQuad() {
        GL.PushMatrix();
        
        // Use the floor mat to draw the quad
        m_FloorMaterial.SetPass(0);
        m_FloorMaterial.SetTexture("_ReflectionTex", m_RenderTarget);

        GL.LoadOrtho();

        GL.Begin(GL.QUADS);

        GL.TexCoord2(1f, 0f);
        GL.Vertex3(0f, 0f, 0f);

        GL.TexCoord2(1f, 1f);
        GL.Vertex3(0f, 1f, 0f);

        GL.TexCoord2(0f, 1f);
        GL.Vertex3(1f, 1f, 0f);

        GL.TexCoord2(0f, 0f);
        GL.Vertex3(1f, 0f, 0f);

        GL.End();

        GL.PopMatrix();
    }
}
